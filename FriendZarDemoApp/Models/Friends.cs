﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FriendZarDemoApp.Models
{
    public class Friends
    {
        public int FriendUid { get; set; }
        public Person Friend { get; set; }
    }
}
