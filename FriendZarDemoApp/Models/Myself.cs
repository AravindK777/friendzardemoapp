﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FriendZarDemoApp.Models
{
    public class Myself
    {
        public int MyUid { get; set; }
        public string MyFullName { get; set; }
        public string MyContactInfo { get; set; }
        public string MyContactType { get; set; }
        public IEnumerable<Friends> MyFriends { get; set; }
    }
}
