﻿namespace FriendZarDemoApp.Models
{
    public class Person
    {
        public int Uid { get; set; }
        public string FullName { get; set; }
        public string ContactInfo { get; set; }
        public string ContactType { get; set; }
    }
}
