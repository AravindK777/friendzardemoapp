﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FriendZarDemoApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FriendZarDemoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FriendsZarController : ControllerBase
    {
        [HttpGet("Friends/{MyUid}", Name = "Get Friends")]
        public IActionResult GetFriends(int MyUid)
        {
            if (MyUid == 0) return BadRequest("Invalid UserId");
            var data = new List<Friends>();
            return Ok(data);
        }

        [HttpPost]
        public IActionResult CreatePersona([FromBody] Myself profile)
        {
            //add the friend
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPost("Friends/{MyUid}", Name = "Create Friend")]
        public IActionResult AddFriend([FromBody] Friends newFriend, int MyUid)
        {
            //add the friend
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("Friends/{MyUid}", Name = "Remove a friend")]
        public IActionResult UnFriend([FromBody] Friends newFriend, int MyUid)
        {
            //add the friend
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpGet("{name}")]
        public  IActionResult Get(string name)
        {
            var myProfile = new Person();
            myProfile.FullName = name;
            myProfile.ContactInfo = "test@testing.me";
            return Ok(myProfile);
        }
    }
}