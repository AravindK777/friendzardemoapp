﻿namespace FriendZarDemoApp.ClientLib.Models
{
    public class FriendsModel
    {
        public int FriendUid { get; set; }
        public string FriendName { get; set; }
        public string FriendContactInfo { get; set; }
    }
}
