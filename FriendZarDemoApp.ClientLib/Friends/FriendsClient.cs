﻿using FriendZarDemoApp.ClientLib.Client;
using FriendZarDemoApp.ClientLib.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FriendZarDemoApp.ClientLib.Friends
{
    public class FriendsClient : IFriendsClient
    {
        private IApiClient<FriendsModel> _apiClient;

        public FriendsClient()
        {
            _apiClient = new ApiClient<FriendsModel>("http://localhost:57786/api/FriendsZar/");
        }

        // Use this during IoC
        public FriendsClient(IApiClient<FriendsModel> client)
        {
            _apiClient = client;
        }

        public async System.Threading.Tasks.Task<IEnumerable<FriendsModel>> GetFriendsAsync(int myUserId)
        {
            var request = $"Friends/{myUserId}";
            var friendsList = await _apiClient.GetAsync(request);
            return friendsList;
        }
    }
}
