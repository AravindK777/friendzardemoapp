﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FriendZarDemoApp.ClientLib.Models;

namespace FriendZarDemoApp.ClientLib.Friends
{
    public interface IFriendsClient
    {
        Task<IEnumerable<FriendsModel>> GetFriendsAsync(int myUserId);
    }
}