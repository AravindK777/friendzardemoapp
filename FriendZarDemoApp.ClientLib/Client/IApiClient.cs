﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FriendZarDemoApp.ClientLib.Client
{
    public interface IApiClient<T> where T:class
    {
        Task<IEnumerable<T>> GetAsync(string requestUri);
        Task<T> GetSingleAsync(string requestUri);
    }
}