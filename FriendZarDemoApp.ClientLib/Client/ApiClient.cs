﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FriendZarDemoApp.ClientLib.Client
{
    public class ApiClient<T> : IApiClient<T> where T: class
    {
        private HttpClient _apiClient;

        public ApiClient(string baseAddress)
        {
            _apiClient = new HttpClient();
            _apiClient.BaseAddress = new Uri(baseAddress);
        }

        public async Task<IEnumerable<T>> GetAsync(string requestUri)
        {
            List<T> result = null;
            var resp = await _apiClient.GetAsync($"Friends/{requestUri}", HttpCompletionOption.ResponseContentRead);
            if (resp != null && resp.IsSuccessStatusCode)
            {
                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = JsonConvert.DeserializeObject<List<T>>(await resp.Content.ReadAsStringAsync());
                }
            }
            return result;
        }        

        public async Task<T> GetSingleAsync(string requestUri)
        {
            T result = null;
            var resp = await _apiClient.GetAsync($"{requestUri}", HttpCompletionOption.ResponseContentRead);
            if (resp != null && resp.IsSuccessStatusCode)
            {
                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = JsonConvert.DeserializeObject<T>(await resp.Content.ReadAsStringAsync());
                }
            }
            return result;
        }
    }
}
