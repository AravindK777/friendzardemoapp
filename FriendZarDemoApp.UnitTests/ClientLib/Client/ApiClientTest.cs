﻿using FriendZarDemoApp.ClientLib.Client;
using FriendZarDemoApp.ClientLib.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FriendZarDemoApp.UnitTests.ClientLib.Client
{
    public class ApiClientTest
    {
        private IApiClient<FriendsModel> _testApiClient;
        public ApiClientTest()
        {
            //arrange globally from Mockapi client class
            _testApiClient = new MockApiClient<FriendsModel>();
        }

        [Fact]
        public void Test_For_GetAsync()
        {
            //var _testClient = new Mock<IApiClient<FriendsModel>>();
            //_testClient.Setup(s => s.GetAsync(It.IsAny<string>())).ReturnsAsync(() => GetMockFriendsList());
            var testData = _testApiClient.GetAsync(It.IsAny<string>()).Result;

            Assert.NotNull(testData);
            Assert.IsAssignableFrom<IEnumerable<FriendsModel>>(testData); 
        }

        [Fact]
        public void Test_For_GetSingleAsync()
        {
            //act
            var testData = _testApiClient.GetSingleAsync(It.IsAny<string>());

            //assert
            Assert.NotNull(testData);
            Assert.IsType<FriendsModel>(testData);
        }
    }
}
