﻿using FriendZarDemoApp.ClientLib.Client;
using FriendZarDemoApp.ClientLib.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FriendZarDemoApp.UnitTests.ClientLib.Client
{
    public class MockApiClient<T> : IApiClient<T> where T : class, new()
    {
        //public async Task<IEnumerable<string>> GetFriendsAsync(int myUserId)
        //{
        //    return await Task.Run(() => new List<string>() { "One", "Two", "Three" });
        //}
        public async Task<IEnumerable<T>> GetAsync(string requestUri)
        {
            return await Task.Run(() => new List<T> { new T(), new T() });
        }

        public async Task<T> GetSingleAsync(string requestUri)
        {
            return await Task.Run(() => new T());
        }        
    }
}
