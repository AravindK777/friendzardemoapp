﻿using FriendZarDemoApp.ClientLib.Client;
using FriendZarDemoApp.ClientLib.Friends;
using FriendZarDemoApp.ClientLib.Models;
using FriendZarDemoApp.UnitTests.ClientLib.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FriendZarDemoApp.UnitTests.ClientLib.Friends
{
    public class FriendsClientTest
    {
        private IFriendsClient testFriendsClient;
        private Mock<IApiClient<FriendsModel>> mockApiClient;

        public FriendsClientTest()
        {
            mockApiClient = new Mock<IApiClient<FriendsModel>>(); //new MockApiClient<FriendsModel>();
            testFriendsClient = new FriendsClient(mockApiClient.Object);
        }

        [Fact]
        public async System.Threading.Tasks.Task Test_GetFriendsListAsync()
        {
            //arrange
            var myUid = 1;
            var request = $"Friends/{myUid}";
            mockApiClient.Setup(req=> req.GetAsync(request)).ReturnsAsync(GetMockFriendsList());

            //act
            var actual = await testFriendsClient.GetFriendsAsync(1);

            //assert
            Assert.Equal(GetMockFriendsList().Count, actual.Count());
        }

        private List<FriendsModel> GetMockFriendsList()
        {
            return new List<FriendsModel>
            {
                new FriendsModel { FriendName = "Friend1", FriendContactInfo = "friend1@test.me", FriendUid = 1001 },
                new FriendsModel { FriendName = "Friend2", FriendContactInfo = "friend2@test.me", FriendUid = 1002 }
            };
        }
    }
}
